data "digitalocean_project" "default_project" {
  name = var.default_project_name
}

data "digitalocean_ssh_key" "default_ssh_key" {
  name = var.default_ssh_key_name
}

// FRONT END
resource "digitalocean_project_resources" "default_project_frontend_resources" {
  count   = var.frontend_count
  project = data.digitalocean_project.default_project.id
  resources = [
    digitalocean_droplet.frontend[count.index].urn
  ]
}

resource "digitalocean_droplet" "frontend" {
  count    = var.frontend_count
  image    = var.default_image
  name     = "Frontend"
  region   = var.default_region
  size     = var.frontend_default_size
  ssh_keys = [data.digitalocean_ssh_key.default_ssh_key.id]
  tags     = ["kreezus", "sirh", "terraform", "frontend"]
}

// STAFF SERVICE

resource "digitalocean_project_resources" "default_project_staff_service_resources" {
  count   = var.staff_service_count
  project = data.digitalocean_project.default_project.id
  resources = [
    digitalocean_droplet.staff_service[count.index].urn
  ]
}

resource "digitalocean_droplet" "staff_service" {
  count    = var.staff_service_count
  image    = var.default_image
  name     = "StaffService"
  region   = var.default_region
  size     = var.staff_service_default_size
  ssh_keys = [data.digitalocean_ssh_key.default_ssh_key.id]
  tags     = ["kreezus", "sirh", "terraform", "staffservice", "microservice"]
}

// Mail SERVICE

resource "digitalocean_project_resources" "default_project_mail_service_resources" {
  count   = var.mail_service_count
  project = data.digitalocean_project.default_project.id
  resources = [
    digitalocean_droplet.mail_service[count.index].urn
  ]
}

resource "digitalocean_droplet" "mail_service" {
  count    = var.mail_service_count
  image    = var.default_image
  name     = "mailervice"
  region   = var.default_region
  size     = var.mail_service_default_size
  ssh_keys = [data.digitalocean_ssh_key.default_ssh_key.id]
  tags     = ["kreezus", "sirh", "terraform", "mailservice", "microservice"]
}

// API GATEWAY

resource "digitalocean_project_resources" "default_project_api_gateway_resources" {
  count   = var.api_gateway_count
  project = data.digitalocean_project.default_project.id
  resources = [
    digitalocean_droplet.api_gateway[count.index].urn
  ]
}

resource "digitalocean_droplet" "api_gateway" {
  count    = var.api_gateway_count
  image    = var.default_image
  name     = "ApiGateway"
  region   = var.default_region
  size     = var.api_gateway_default_size
  ssh_keys = [data.digitalocean_ssh_key.default_ssh_key.id]
  tags     = ["kreezus", "sirh", "terraform", "apigateway", "microservice"]
}
