variable "default_project_name" {
  type    = string
  default = "Kreezus"
}

variable "default_ssh_key_name" {
  type    = string
  default = "KREEZUS-SIRH"
}

variable "default_image" {
  type    = string
  default = "ubuntu-20-04-x64"
}

variable "default_region" {
  type    = string
  default = "fra1"
}

variable "default_size" {
  type    = string
  default = "s-1vcpu-2gb"
}

// Front End variables
variable "frontend_count" {
  type    = number
  default = 1
}

variable "frontend_default_size" {
  type    = string
  default = "s-1vcpu-2gb"
}
// Staff service variables
variable "staff_service_count" {
  type    = number
  default = 1
}

variable "staff_service_default_size" {
  type    = string
  default = "s-1vcpu-2gb"
}

// Mail service variables
variable "mail_service_count" {
  type    = number
  default = 1
}

variable "mail_service_default_size" {
  type    = string
  default = "s-1vcpu-2gb"
}

// API Gateway variables
variable "api_gateway_count" {
  type    = number
  default = 1
}
variable "api_gateway_default_size" {
  type    = string
  default = "s-1vcpu-2gb"
}
