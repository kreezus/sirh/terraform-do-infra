terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.8.0"
    }
  }
  # backend "http" {
  # }
}

variable "do_token" {}

provider "digitalocean" {
  # Configuration options
  token = var.do_token
}

